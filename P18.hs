slice xs n m 
    | n < 0 = error "N was less than 0"
    | otherwise = take (m-nPred) $ drop nPred xs
    where
        nPred = n-1
