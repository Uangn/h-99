import Data.List (group, foldl1')
data OneOrMore a = Single a | Multiple Int a
    deriving (Show)

encodeDirect :: Eq a => [a] -> [OneOrMore a]
encodeDirect (x:xs) = go 1 x xs
    where
    go :: Eq a => Int -> a -> [a] -> [OneOrMore a]
    go n a [] = [oneOrMore n a]
    go n a (x:xs)
        | a == x    = go (n+1) a xs
        | otherwise = oneOrMore n a : go 1 x xs

    oneOrMore n x
        | n == 1    = Single x
        | otherwise = Multiple n x
