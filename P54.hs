module P54 (Tree(..)) where

import Control.Monad ((<=<))

data Tree a = Empty | Branch a (Tree a) (Tree a)
              deriving (Show, Eq, Ord)


leaf x = Branch x Empty Empty
treeX = leaf "X"
branchX = Branch "X"

addToFirstEmpties :: Tree a -> Tree a -> [Tree a]
addToFirstEmpties a Empty = [a]
addToFirstEmpties a (Branch v Empty Empty) = [Branch v Empty a, Branch v a Empty]
addToFirstEmpties a (Branch v Empty r) = [Branch v a r]
addToFirstEmpties a (Branch v l Empty) = [Branch v l a]
addToFirstEmpties a (Branch v l r) =
    map (flip (Branch v) r) (addToFirstEmpties a l) ++ map (Branch v l) (addToFirstEmpties a r)

cbalTree :: Int -> Tree String -> [Tree String]
cbalTree 0 = (: [])
cbalTree n = cbalTree (n-1) <=< f
    where
    f = addToFirstEmpties treeX

distinct :: Eq a => [a] -> [a]
distinct (x:xs) = snd $ foldl (\(p, a) x -> if x /= p then (x, x:a) else (p,a)) (x, [x]) xs

sort [] = []
sort (x:xs) = sort lt <> [x] <> sort gt
    where
    lt = [y | y <- xs, y < x]
    gt = [y | y <- xs, y >= x]

test n = mapM_ (print . pr 1) $ distinct $ sort $ cbalTree n Empty
    where
    pr n Empty = ""
    pr n (Branch x l r) = concat [show n, "<l", pr (n+1) l, "> <r", pr (n+1) r, ">"]
