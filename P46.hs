tableN space f = [ (a, b, f a b)
                 | a <- space
                 , b <- space]

tableN' space f = map (\a b -> (a, b, f a b)) space <*> space

tableN'' :: [t] -> (t -> t -> c) -> [(t, t, c)]
tableN'' space f = do
    a <- space
    b <- space
    pure (a, b, f a b)

table = tableN'' [True,False]

and' = (&&)
or' = (||)
