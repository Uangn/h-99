a `divisible` b = a `mod` b == 0

isPrime x = x > 1 && go 2
    where
        go n
            | x == n = True
            | x `mod` n == 0 = False
            | otherwise = go (n+1)

isPrime' x
    | x <= 1    = False
    | otherwise = not $ any (x `divisible`) [2..x-1]

isPrime'' x = x > 1 && go 2
    where
        go n
            | x == n          = True
            | squared > n     = not $ x `divisible` n
            | squared == n    = False
            | x `divisible` n = False
            | otherwise       = go (n+1)
            where squared = n * n
