-- I literally can't with IO, so I'm just gonna do permutations


permu :: [a] -> [[a]]
permu []  = []
permu [x] = [[x]]
permu (x:xs) = insertThroughout x =<< permu xs


insertThroughout :: a -> [a] -> [[a]]
insertThroughout x = go []
    where
    go ls []     = [ls<>[x]]
    go ls (r:rs) = (ls<>(x:r:rs)) : go (ls<>[r]) rs
