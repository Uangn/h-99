lsort :: (Ord a) => [[a]] -> [[a]]
lsort = map fst . sortOn snd . map (\x -> (x, length x))

lfsort :: Foldable t => [t a] -> [t a]
lfsort = map snd
       . concat
       . sortOn length
       . groupBy fst
       . sortOn fst
       . map (\x -> (length x, x))


sortOn :: Ord b => (a -> b) -> [a] -> [a]
sortOn _ [] = []
sortOn f (x:xs) = lt <> [x] <> gt
    where (lt,gt) = partitionOn f (< f x) xs

partitionOn :: (a -> b) -> (b -> Bool) -> [a] -> ([a], [a])
partitionOn f p = foldl go ([], [])
    where
    go (l,r) x =
        if p $ f x
            then (x:l,r)
            else (l,x:r)

groupBy :: Eq b => (a -> b) -> [a] -> [[a]]
groupBy f = foldr go []
    where
    go x []           = [[x]]
    go x ((ha:ta):as) =
        if f x == f ha
            then (x:ha:ta):as
            else [x]:(ha:ta):as
