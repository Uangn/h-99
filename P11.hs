import Data.List (group)
data OneOrMore a = Single a | Multiple Int a
    deriving (Show)

encodeModified :: Eq a => [a] -> [OneOrMore a]
encodeModified = map getPairedOrSingle . group
    where
    getPairedOrSingle :: [a] -> OneOrMore a
    getPairedOrSingle xs
        | length xs == 1 = Single $ head xs
        | otherwise      = Multiple (length xs) (head xs)
