-- [Multiple 4 'a',Single 'b',Multiple 2 'c',Multiple 2 'a',Single 'd',Multiple 4 'e']
data OneOrMore a = Single a | Multiple Int a
    deriving (Show)

decodeModified :: [OneOrMore a] -> [a]
decodeModified xs = xs >>= decode
    where
    decode (Single x) = [x]
    decode (Multiple n x) = replicate n x 
