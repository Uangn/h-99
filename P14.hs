duplicate :: Foldable t => t a -> [a]
duplicate = concatMap (replicate 2)
