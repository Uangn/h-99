range x y = [x..y]


range' n m
    | n <= m = go n m
    where
    go x y
        | x == y    = [y]
        | otherwise = x : range' (x+1) y


range'' x y = takeWhile (/=y+1) $ iterate (+1) x


range''' x y = take (y-x+1) $ iterate (+1) x


range'''' x y = take (y-x+1) $ scanl1 (const . (+1)) $ repeat x
