import Data.Function (on)
import Data.Char (toLower)

halve :: [a] -> ([a], [a])
halve xs = splitAt (length xs `div` 2) xs

isPalindrome :: Eq a => [a] -> Bool
isPalindrome xs
    | even lenXS = reverse left == right
    | otherwise  = reverse left == tail right
    where
        lenXS = length xs
        (left, right) = halve xs

-- Strings
(=~=) :: String -> String -> Bool
(=~=) = (==) `on` map toLower

