import P54 (Tree(..))

tree4 = Branch 1 (Branch 2 Empty (Branch 4 Empty Empty))
                 (Branch 2 Empty Empty)

instance Foldable Tree where
    foldr :: (a -> b -> b) -> b -> Tree a -> b
    foldr f a (Branch x Empty Empty) = f x a
    foldr f a (Branch x l Empty) = f x (foldr f a l)
    foldr f a (Branch x Empty r) = f x (foldr f a r)
    foldr f a (Branch x l r) = foldr f (f x (foldr f a r)) l

f = countLeaves tree4

countLeaves :: Num a => Tree a -> a
countLeaves = foldLeaves (+) (const 1)

leaves :: Tree a -> [a]
leaves = foldLeaves (<>) (:[])

foldLeaves :: (a -> a -> a) -> (b -> a) -> Tree b -> a
foldLeaves f a (Branch x Empty Empty) = a x
foldLeaves f a (Branch x l Empty) = foldLeaves f a l
foldLeaves f a (Branch x Empty r) = foldLeaves f a r
foldLeaves f a (Branch x l r) = foldLeaves f a l `f` foldLeaves f a r
