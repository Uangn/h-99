repli :: Foldable t => Int -> t a -> [a]
repli n = concatMap (replicate n)

repli' :: Foldable t => t a -> Int -> [a]
repli' = flip repli
