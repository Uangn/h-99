insertAt :: (Eq b, Num b) => a -> [a] -> b -> [a]
insertAt a (x:xs) 1 = a:x:xs
insertAt a (x:xs) 2 = x:a:xs
insertAt a (x:xs) n = x : insertAt a xs (n-1)


insertAt' :: a -> [a] -> Int -> [a]
insertAt' a xs n =
    let (l,r) = splitAt (n-1) xs
    in l <> (a : r)


insertAt'' :: (Num p, Enum p, Eq p) => a -> [a] -> p -> [a]
insertAt'' a xs n = reverse $ foldl go [] $ zip [0..] xs
    where
    go ls (i, x)
        | i == (n-1) = x : a : ls
        | otherwise  = x : ls
