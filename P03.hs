elementAt :: [a] -> Int -> a
elementAt [] _ = error "Empty List"
elementAt (x:_) 1 = x
elementAt (_:xs) n 
    | n < 1 = error "Negative Search"
    | otherwise = elementAt xs (n-1)
