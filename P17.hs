split :: [a] -> Int -> ([a], [a])
split = flip $ split' []
    where
    split' acc _ [] = (acc, [])
    split' acc n (x:xs)
        | n <= 0     = (reverse acc, x:xs)
        | otherwise = split' (x:acc) (n-1) xs
