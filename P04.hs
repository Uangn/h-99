import Data.List (foldl')
myLength :: [a] -> Integer
myLength = foldl' (const . (+1)) 0
