removeAt :: Int -> [a] -> (a, [a])
removeAt n xs
    | n <= 0     = error "N less than equal 0"
    | otherwise = removeAt' [] (n-1) xs
    where
    removeAt' _ _ []       = error "N longer than list"
    removeAt' acc 0 (x:xs) = (x, reverse acc ++ xs)
    removeAt' acc n (x:xs) = removeAt' (x:acc) (n-1) xs
