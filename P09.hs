pack :: Eq a => [a] -> [[a]]
pack [] = []
pack (x:xs) = (x : f) : pack e
    where (f,e) = span (==x) xs
    --where (f,e) = (takeWhile (==x) xs, dropWhile (==x) xs)
