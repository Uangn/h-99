dropEvery :: [a] -> Int -> [a]
dropEvery xs m = map fst 
               $ filter ((/=m) . snd)
               $ zip xs
               $ cycle [1..m]

-- dropEvery :: [a] -> Int -> [a]
-- dropEvery xs m = reverse $ dropEvery' [] m xs
--     where
--     dropEvery' a _ []     = a
--     dropEvery' a 1 (x:xs) = dropEvery' a m xs
--     dropEvery' a n (x:xs) = dropEvery' (x:a) (n-1) xs
