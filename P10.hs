import Data.List (group)
encode :: Eq a => [a] -> [(Int, a)]
encode = map (\xs -> (length xs, head xs)) . group

-- encode :: Eq a => [a] -> [(Int, a)]
-- encode [] = []
-- encode (x:xs) = (length f + 1, x) : encode e
--     where (f,e) = span (==x) xs
