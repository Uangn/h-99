myButLast :: [a] -> a
myButLast = (!! 1) . reverse


-- myButLast :: [a] -> [a]
-- myButLast [] = error "Empty List"
-- myButLast (x:_:[]) = x
-- myButLast (x:xs) = myButLast xs
