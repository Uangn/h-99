import Data.List (foldl1')

myLast :: [a] -> a
myLast = foldl1' seq

-- myLast :: [a] -> a
-- myLast []     = error "Empty List"
-- myLast [x]    = x
-- myLast (_:xs) = myLast xs

